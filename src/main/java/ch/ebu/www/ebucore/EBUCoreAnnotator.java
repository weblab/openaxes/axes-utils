package ch.ebu.www.ebucore;

import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.Value;

/**
 * Simple annotator for EBUCore specific properties.
 *
 * @author lserrano
 * @date 2012-08-30
 */

public class EBUCoreAnnotator extends BaseAnnotator {


	protected static final String EBUCORE_PREFIX = "ebucore";


	protected static final URI EBUCORE_URI = URI.create("http://www.ebu.ch/metadata/ontologies/ebucore#");


	/**
	 * @param subject
	 *            The inner annotator subject
	 * @param pieceOfKnowledge
	 *            The PoK into which this annotator should write
	 */
	public EBUCoreAnnotator(final URI subject, final PieceOfKnowledge pieceOfKnowledge) {
		super(subject, pieceOfKnowledge);
	}


	/**
	 * @param resource
	 *            the resource to be read and written
	 */
	public EBUCoreAnnotator(final Resource resource) {
		super(resource);
	}


	protected static final String MAIN_TITLE = "mainTitle";


	protected static final String HAS_PUB_EVENT = "hasPublicationEvent";


	protected static final String HAS_COLOUR_SPACE = "hasColourSpace";


	protected static final String HAS_KEYWORD = "hasKeyword";


	protected static final String HAS_SUBJECT = "hasSubject";


	protected static final String HAS_GENRE = "hasGenre";


	protected static final String HAS_CONTRIBUTOR = "hasContributor";


	protected static final String IS_COVERED_BY = "isCoveredBy";


	protected static final String SUMMARY = "summary";


	protected static final String PUBLISHED = "publishedStartDateTime";


	protected static final String HAS_PUBLI_CHANNEL = "hasPublicationChannel";


	protected static final String KEYWORD_NAME = "keywordName";


	protected static final String GENRE_NAME = "genreName";


	protected static final String NAME = "name";


	protected static final String HAS_ROLE = "hasRole";


	protected static final String HAS_RIGHTS_HOLDER = "hasRightsHolder";


	/**
	 * Read the "mainTitle" in the EBUCore ontology.
	 *
	 * @return a String
	 */
	public Value<String> readMainTitle() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.MAIN_TITLE, String.class, null);
	}


	/**
	 * Write the "mainTitle" in the EBUCore ontology.
	 *
	 * @param value
	 *            a String
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeMainTitle(final String value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.MAIN_TITLE, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the "publicationEvent" in the EBUCore ontology.
	 *
	 * @return an URI
	 */
	public Value<URI> readPubliEvent() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_PUB_EVENT, URI.class, null);
	}


	/**
	 * Write the "publicationEvent" in the EBUCore ontology.
	 *
	 * @param value
	 *            an URI
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writePubliEvent(final URI value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_PUB_EVENT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the "colourSpace" in the EBUCore ontology.
	 *
	 * @return an URI
	 */
	public Value<URI> readColourSpace() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_COLOUR_SPACE, URI.class, null);
	}


	/**
	 * Write the "colourSpace" in the EBUCore ontology.
	 *
	 * @param value
	 *            an URI
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeColourSpace(final URI value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_COLOUR_SPACE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the "keyword" in the EBUCore ontology.
	 *
	 * @return an URI
	 */
	public Value<URI> readKeyword() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_KEYWORD, URI.class, null);
	}


	/**
	 * Write the "keyword" in the EBUCore ontology.
	 *
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 * @param value
	 *            an URI
	 */
	public PieceOfKnowledge writeKeyword(final URI value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_KEYWORD, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the "subject" in the EBUCore ontology.
	 *
	 * @return an URI
	 */
	public Value<URI> readSubject() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_SUBJECT, URI.class, null);
	}


	/**
	 * Write the "subject" in the EBUCore ontology.
	 *
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 * @param value
	 *            an URI
	 */
	public PieceOfKnowledge writeSubject(final URI value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_SUBJECT, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the "genre" in the EBUCore ontology.
	 *
	 * @return an URI
	 */
	public Value<URI> readGenre() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_GENRE, URI.class, null);
	}


	/**
	 * Write the "genre" in the EBUCore ontology.
	 *
	 * @param value
	 *            an URI
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeGenre(final URI value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_GENRE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the "contributor" in the EBUCore ontology.
	 *
	 * @return an URI
	 */
	public Value<URI> readContributor() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_CONTRIBUTOR, URI.class, null);
	}


	/**
	 * Write the "contributor" in the EBUCore ontology.
	 *
	 * @param value
	 *            an URI
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeContributor(final URI value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_CONTRIBUTOR, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the "isCoveredBy" in the EBUCore ontology.
	 *
	 * @return an URI
	 */
	public Value<URI> readIsCoveredBy() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.IS_COVERED_BY, URI.class, null);
	}


	/**
	 * Write the "isCoveredBy" in the EBUCore ontology.
	 *
	 * @param value
	 *            an URI
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeIsCoveredBy(final URI value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.IS_COVERED_BY, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the "summary" in the EBUCore ontology.
	 *
	 * @return a String
	 */
	public Value<String> readSummary() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.SUMMARY, String.class, null);
	}


	/**
	 * Write the "summary" in the EBUCore ontology.
	 *
	 * @param value
	 *            a String
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeSummary(final String value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.SUMMARY, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the "publishedStartDateTime" in the EBUCore ontology.
	 *
	 * @return a Date
	 */
	public Value<Date> readPublished() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.PUBLISHED, Date.class, null);
	}


	/**
	 * Write the "publishedStartDateTime" in the EBUCore ontology.
	 *
	 * @param value
	 *            a Date
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writePublished(final Date value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.PUBLISHED, Date.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the "publicationChannel" in the EBUCore ontology.
	 *
	 * @return an URI
	 */
	public Value<URI> readPubliChannel() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_PUBLI_CHANNEL, URI.class, null);
	}


	/**
	 * Write the "publicationChannel" in the EBUCore ontology.
	 *
	 * @param value
	 *            an URI
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writePubliChannel(final URI value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_PUBLI_CHANNEL, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the "keywordName" in the EBUCore ontology.
	 *
	 * @return a String
	 */
	public Value<String> readKeywordName() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.KEYWORD_NAME, String.class, null);
	}


	/**
	 * Write the "keywordName" in the EBUCore ontology.
	 *
	 * @param value
	 *            a String
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeKeywordName(final String value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.KEYWORD_NAME, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the "genreName" in the EBUCore ontology.
	 *
	 * @return a String
	 */
	public Value<String> readGenreName() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.GENRE_NAME, String.class, null);
	}


	/**
	 * Write the "genreName" in the EBUCore ontology.
	 *
	 * @param value
	 *            a String
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeGenreName(final String value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.GENRE_NAME, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the "name" in the EBUCore ontology.
	 *
	 * @return a String
	 */
	public Value<String> readName() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.NAME, String.class, null);
	}


	/**
	 * Write the "name" in the EBUCore ontology.
	 *
	 * @param value
	 *            a String
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeName(final String value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.NAME, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the "role" in the EBUCore ontology.
	 *
	 * @return an URI
	 */
	public Value<URI> readRole() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_ROLE, URI.class, null);
	}


	/**
	 * Write the "role" in the EBUCore ontology.
	 *
	 * @param value
	 *            an URI
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeRole(final URI value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_ROLE, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the "rightsHolder" in the EBUCore ontology.
	 *
	 * @return an URI
	 */
	public Value<URI> readRightsHolder() {
		return this.applyOperator(Operator.READ, null, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_RIGHTS_HOLDER, URI.class, null);
	}


	/**
	 * Write the "rightsHolder" in the EBUCore ontology.
	 *
	 * @param value
	 *            an URI
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeRightsHolder(final URI value) {
		return this.applyOperator(Operator.WRITE, EBUCoreAnnotator.EBUCORE_PREFIX, EBUCoreAnnotator.EBUCORE_URI, EBUCoreAnnotator.HAS_RIGHTS_HOLDER, URI.class, value).getIsAnnotatedOn();
	}


	/**
	 * Retrieves a set of the main titles of a video
	 *
	 * @return a Set of String
	 */
	public Set<String> getMainTitles() {
		Set<String> mainTitles = new HashSet<>();
		final Value<String> mainTitlesValues = this.readMainTitle();
		if (mainTitlesValues.hasValue()) {
			for (final String mainTitleValue : mainTitlesValues.getValues()) {
				mainTitles.add(mainTitleValue);
			}
		}
		return mainTitles;
	}


	/**
	 * Retrieves a set of the summaries of a video
	 *
	 * @return a Set of String
	 */
	public Set<String> getSummaries() {
		Set<String> summaries = new HashSet<>();
		final Value<String> summariesValues = this.readSummary();
		if (summariesValues.hasValue()) {
			for (final String summaryValue : summariesValues.getValues()) {
				summaries.add(summaryValue);
			}
		}
		return summaries;
	}


	/**
	 * Retrieves a set of the subjects of a video
	 *
	 * @return a Set of String
	 */
	public Set<String> getSubjects() {
		Set<String> subjects = new HashSet<>();
		final Value<URI> subjectsValues = this.readSubject();
		if (subjectsValues.hasValue()) {
			for (final URI subjectUri : subjectsValues.getValues()) {
				this.startInnerAnnotatorOn(subjectUri);
				Value<String> subjectLabelValues = this.readLabel();
				if (subjectLabelValues.hasValue()) {
					subjects.addAll(subjectLabelValues.getValues());
				} else {
					subjectLabelValues = this.readName();
					if (subjectLabelValues.hasValue()) {
						subjects.addAll(subjectLabelValues.getValues());
					}
				}
				this.endInnerAnnotator();
			}
		}
		return subjects;
	}


	/**
	 * Retrieves a set of the keywords of a video
	 *
	 * @return a Set of String
	 */
	public Set<String> getKeywords() {
		Set<String> keywords = new HashSet<>();
		final Value<URI> keywordsValues = this.readKeyword();
		if (keywordsValues.hasValue()) {
			for (final URI keywordUri : keywordsValues.getValues()) {
				this.startInnerAnnotatorOn(keywordUri);
				Value<String> keywordNameValues = this.readKeywordName();
				if (keywordNameValues.hasValue()) {
					keywords.addAll(keywordNameValues.getValues());
				} else {
					keywordNameValues = this.readLabel();
					if (keywordNameValues.hasValue()) {
						keywords.addAll(keywordNameValues.getValues());
					}
				}
				this.endInnerAnnotator();
			}
		}
		return keywords;
	}


	/**
	 * Retrieves a set of the genres of a video
	 *
	 * @return a Set of String
	 */
	public Set<String> getGenres() {
		Set<String> genres = new HashSet<>();
		final Value<URI> genresValues = this.readGenre();
		if (genresValues.hasValue()) {
			for (final URI genreUri : genresValues.getValues()) {
				this.startInnerAnnotatorOn(genreUri);
				Value<String> genreNameValues = this.readGenreName();
				if (genreNameValues.hasValue()) {
					genres.addAll(genreNameValues.getValues());
				} else {
					genreNameValues = this.readLabel();
					genres.addAll(genreNameValues.getValues());
				}
				this.endInnerAnnotator();
			}
		}
		return genres;
	}


	/**
	 * Retrieves a set of the colour spaces of a video
	 *
	 * @return a Set of String
	 */
	public Set<String> getColourSpaces() {
		Set<String> colourSpaces = new HashSet<>();
		final Value<URI> colourSpacesValues = this.readColourSpace();
		if (colourSpacesValues.hasValue()) {
			for (final URI colourSpaceUri : colourSpacesValues.getValues()) {
				this.startInnerAnnotatorOn(colourSpaceUri);
				final Value<String> colourLabelValues = this.readLabel();
				if (colourLabelValues.hasValue()) {
					colourSpaces.addAll(colourLabelValues.getValues());
				}
				this.endInnerAnnotator();
			}
		}
		return colourSpaces;
	}


	/**
	 * Retrieves a set of the publication events of a video
	 *
	 * @return a Map of (String publication date, Set of String publication channels)
	 */
	public Map<Date, Set<String>> getPubliEvents() {
		Map<Date, Set<String>> publiEvents = new HashMap<>();
		final Value<URI> publiEventsValues = this.readPubliEvent();
		for (final URI publiEventUri : publiEventsValues.getValues()) {
			this.startInnerAnnotatorOn(publiEventUri);
			final Value<URI> publiChannelValues = this.readPubliChannel();
			final Value<Date> publiDateValues = this.readPublished();
			Set<String> publiChannelLabels = new HashSet<>();
			for (final URI publiChannelUri : publiChannelValues.getValues()) {
				this.startInnerAnnotatorOn(publiChannelUri);
				final Value<String> publiChannelLabelValues = this.readLabel();
				if (publiChannelLabelValues.hasValue()) {
					publiChannelLabels.addAll(publiChannelLabelValues.getValues());
				}
				this.endInnerAnnotator();
			}
			for (final Date date : publiDateValues) {
				if (!publiEvents.containsKey(date)) {
					publiEvents.put(date, new HashSet<String>());
				}
				publiEvents.get(date).addAll(publiChannelLabels);
			}
			this.endInnerAnnotator();
		}
		return publiEvents;
	}


	/**
	 * Retrieves a set of the publication dates of a video
	 *
	 * @return a Set of Date
	 * @deprecated you should use the getPubliEvents() method
	 * @see #getPubliEvents()
	 */
	@Deprecated
	public Set<Date> getPubliDates() {
		Set<Date> publiDates = new HashSet<>();
		final Value<URI> publiEventsValues = this.readPubliEvent();
		if (publiEventsValues.hasValue()) {
			for (final URI publiEventUri : publiEventsValues.getValues()) {
				this.startInnerAnnotatorOn(publiEventUri);
				final Value<Date> publiDateValues = this.readPublished();
				if (publiDateValues.hasValue()) {
					publiDates.addAll(publiDateValues.getValues());
				}
				this.endInnerAnnotator();
			}
		}
		return publiDates;
	}


	/**
	 * Retrieves a set of the publication channels of a video
	 *
	 * @return a Set of String
	 * @deprecated you should use the getPubliEvents() method
	 * @see #getPubliEvents()
	 */
	@Deprecated
	public Set<String> getPubliChannels() {
		Set<String> publiChannels = new HashSet<>();
		final Value<URI> publiEventsValues = this.readPubliEvent();
		if (publiEventsValues.hasValue()) {
			for (final URI publiEventUri : publiEventsValues.getValues()) {
				this.startInnerAnnotatorOn(publiEventUri);
				final Value<URI> publiChannelValues = this.readPubliChannel();
				if (publiChannelValues.hasValue()) {
					for (final URI publiChannelUri : publiChannelValues.getValues()) {
						this.startInnerAnnotatorOn(publiChannelUri);
						final Value<String> publiChannelLabelValues = this.readLabel();
						if (publiChannelLabelValues.hasValue()) {
							publiChannels.addAll(publiChannelLabelValues.getValues());
						}
						this.endInnerAnnotator();
					}
				}
				this.endInnerAnnotator();
			}
		}
		return publiChannels;
	}


	/**
	 * Retrieves a set of the contributors of a video
	 *
	 * @return a Map of (String contributor's role, Set of String contributors' names)
	 */
	public Map<String, Set<String>> getContributors() {
		Map<String, Set<String>> contributors = new HashMap<>();
		final Value<URI> contributorsValues = this.readContributor();
		for (final URI contributorUri : contributorsValues.getValues()) {
			this.startInnerAnnotatorOn(contributorUri);
			Value<String> contribNameValues = new Value<>();
			if (this.readName().hasValue()) {
				contribNameValues = this.readName();
			} else {
				contribNameValues = this.readLabel();
			}
			final Value<URI> contribRoleValues = this.readRole();
			Set<String> rolesLabels = new HashSet<>();
			for (final URI contribRoleUri : contribRoleValues.getValues()) {
				this.startInnerAnnotatorOn(contribRoleUri);
				final Value<String> roleLabelValues = this.readLabel();
				if (roleLabelValues.hasValue()) {
					rolesLabels.addAll(roleLabelValues.getValues());
				}
				this.endInnerAnnotator();
			}
			for (final String roleLabel : rolesLabels) {
				if (!contributors.containsKey(roleLabel)) {
					contributors.put(roleLabel, new HashSet<String>());
				}
				Set<String> contribNames = contributors.get(roleLabel);
				contribNames.addAll(contribNameValues.getValues());
			}
			this.endInnerAnnotator();
		}
		return contributors;
	}


	/**
	 * Retrieves a set of the contributors' names of a video
	 *
	 * @return a Set of String
	 * @deprecated you should use the getContributors() method
	 * @see #getContributors()
	 */
	@Deprecated
	public Set<String> getContribNames() {
		Set<String> contribNames = new HashSet<>();
		final Value<URI> contributorsValues = this.readContributor();
		if (contributorsValues.hasValue()) {
			for (final URI contributorUri : contributorsValues.getValues()) {
				this.startInnerAnnotatorOn(contributorUri);
				final Value<String> contribNameValues = this.readName();
				if (contribNameValues.hasValue()) {
					contribNames.addAll(contribNameValues.getValues());
				}
				this.endInnerAnnotator();
			}
		}
		return contribNames;
	}


	/**
	 * Retrieves a set of the contributors' roles of a video
	 *
	 * @return a Set of String
	 * @deprecated you should use the getContributors() method
	 * @see #getContributors()
	 */
	@Deprecated
	public Set<String> getContribRoles() {
		Set<String> contribRoles = new HashSet<>();
		final Value<URI> contributorsValues = this.readContributor();
		if (contributorsValues.hasValue()) {
			for (final URI contributorUri : contributorsValues.getValues()) {
				this.startInnerAnnotatorOn(contributorUri);
				final Value<URI> contribRoleValues = this.readRole();
				if (contribRoleValues.hasValue()) {
					for (final URI contribRoleUri : contribRoleValues.getValues()) {
						this.startInnerAnnotatorOn(contribRoleUri);
						final Value<String> roleLabelValues = this.readLabel();
						if (roleLabelValues.hasValue()) {
							contribRoles.addAll(roleLabelValues.getValues());
						}
						this.endInnerAnnotator();
					}
				}
				this.endInnerAnnotator();
			}
		}
		return contribRoles;
	}


	/**
	 * Retrieves a set of of rights the video is covered by
	 *
	 * @return a Map of (String rights holder's name, Set of String rights labels)
	 */
	public Map<String, Set<String>> getRights() {
		Map<String, Set<String>> rights = new HashMap<>();
		final Value<URI> rightsValues = this.readIsCoveredBy();
		for (final URI rightsUri : rightsValues.getValues()) {
			this.startInnerAnnotatorOn(rightsUri);
			final Value<String> rightsLabelValues = this.readLabel();
			final Value<URI> rightsHolderValues = this.readRightsHolder();
			Set<String> rightsHoldersLabels = new HashSet<>();
			for (final URI rightsHolderUri : rightsHolderValues.getValues()) {
				this.startInnerAnnotatorOn(rightsHolderUri);
				Value<String> rightsHolderLabelValues = this.readName();
				if (rightsHolderLabelValues.hasValue()) {
					rightsHoldersLabels.addAll(rightsHolderLabelValues.getValues());
				} else {
					rightsHolderLabelValues = this.readLabel();
					if (rightsHolderLabelValues.hasValue()) {
						rightsHoldersLabels.addAll(rightsHolderLabelValues.getValues());
					}
				}
				this.endInnerAnnotator();
			}
			for (final String rightsHolderLabel : rightsHoldersLabels) {
				if (!rights.containsKey(rightsHolderLabel)) {
					rights.put(rightsHolderLabel, new HashSet<String>());
				}
				rights.get(rightsHolderLabel).addAll(rightsLabelValues.getValues());
			}
			this.endInnerAnnotator();
		}
		return rights;
	}

}