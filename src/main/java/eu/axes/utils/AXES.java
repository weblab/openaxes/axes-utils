package eu.axes.utils;

import java.net.URI;


/**
 * This class is a container for static String used in common by the AXES platform.
 *
 * It contains two inner classes to provide readable access to classes URI vs properties URI.
 *
 *
 * More over it also contains parsing methods for URIs and Limas Ids.
 *
 * @author Cassidian
 * @date 2011-09-12
 */
public class AXES {


	/**
	 * The base URI of the axes project URIs.
	 * <p>{@value}
	 * </p>
	 */
	public static final String BASE_URI = "http://axes-project.eu/";


	/**
	 * An abstract class that only contains static String of Axes specific classes URI (segment, entities...).
	 */
	public abstract static class CLASSES {


		/**
		 * The base URI of the axes project classes URIs.
		 */
		public final static String BASE_CLASSES_URI = AXES.BASE_URI + "classes#";


		/**
		 * The URI of the person class.
		 */
		public final static URI PERSON = URI.create(CLASSES.BASE_CLASSES_URI + "Person");


		/**
		 * The URI of the object class.
		 */
		public final static URI OBJECT = URI.create(CLASSES.BASE_CLASSES_URI + "Object");


		/**
		 * The URI of the place class.
		 */
		public final static URI PLACE = URI.create(CLASSES.BASE_CLASSES_URI + "Place");


		/**
		 * The URI of the organisation class.
		 */
		public final static URI ORGANISATION = URI.create(CLASSES.BASE_CLASSES_URI + "Organisation");


		/**
		 * The URI of the event class.
		 */
		public final static URI EVENT = URI.create(CLASSES.BASE_CLASSES_URI + "Event");


		/**
		 * The URI of the category class.
		 */
		public final static URI CATEGORY = URI.create(CLASSES.BASE_CLASSES_URI + "Category");


		/**
		 * The URI of the shot segment class.
		 */
		public final static URI SHOT_SEGMENT = URI.create(CLASSES.BASE_CLASSES_URI + "ShotSegment");


		/**
		 * The URI of the frame segment class.
		 */
		public final static URI FRAME_SEGMENT = URI.create(CLASSES.BASE_CLASSES_URI + "FrameSegment");


		/**
		 * The URI of the object segment class.
		 */
		public final static URI OBJECT_SEGMENT = URI.create(CLASSES.BASE_CLASSES_URI + "ObjectSegment");


		/**
		 * The URI of the category segment class.
		 */
		public static final URI CATEGO_SEGMENT = URI.create(CLASSES.BASE_CLASSES_URI + "CategoSegment");


		/**
		 * he URI of the speech segment class.
		 */
		public static final URI SPEECH_SEGMENT = URI.create(CLASSES.BASE_CLASSES_URI + "SpeechSegment");


	}


	/**
	 * An abstract class that only contains static String of Axes specific properties URI (hasConfidence...).
	 */
	public abstract static class PROPERTIES {


		/**
		 * The base URI of the axes project properties URIs.
		 * <p>{@value}
		 * </p>
		 */
		public final static String BASE_PROPERTIES_URI = AXES.BASE_URI + "properties#";


		/**
		 * The URI of the has confidence property.
		 * <p>{@value}
		 * </p>
		 */
		public final static URI HAS_CONFIDENCE = URI.create(PROPERTIES.BASE_PROPERTIES_URI + "hasConfidence");

	}


}
