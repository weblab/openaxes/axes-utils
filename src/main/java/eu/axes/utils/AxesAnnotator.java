package eu.axes.utils;

import java.net.URI;

import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.Value;

import eu.axes.utils.AXES.PROPERTIES;


/**
 * Simple annotator for AXES specific properties such as the confidence or the Limas Ids.
 *
 * @author ymombrun
 * @date 2011-11-25
 */
public class AxesAnnotator extends BaseAnnotator {


	private static final URI NS = URI.create(PROPERTIES.BASE_PROPERTIES_URI);


	private static final String PREFIX = "axes";


	private static final String HAS_CONFIDENCE = "hasConfidence";


	private static final String HAS_LIMAS_ID = "hasLimasId";


	private static final String HAS_COLLECTION_ID = "hasCollectionId";


	private static final String HAS_VIDEO_ID = "hasVideoId";


	private static final String HAS_SHOT_ID = "hasShotId";


	private static final String HAS_FRAME_ID = "hasFrameId";


	private static final String HAS_SPEECH_ID = "hasSpeechId";


	private static final String NB_OF_DOCS_TOTAL = "NbDocsTotal";


	/**
	 * @param resource
	 *            The resource to be read and written
	 */
	public AxesAnnotator(final Resource resource) {
		super(resource);
	}


	/**
	 * @param subject
	 *            The default subject of any statement to be read or written
	 * @param pieceOfKnowledge
	 *            The PoK to be read or written
	 */
	public AxesAnnotator(final URI subject, final PieceOfKnowledge pieceOfKnowledge) {
		super(subject, pieceOfKnowledge);
	}


	/**
	 * Read the confidence for a Segment.
	 *
	 * @return a Double
	 */
	public Value<Double> readConfidence() {
		return this.applyOperator(Operator.READ, null, AxesAnnotator.NS, AxesAnnotator.HAS_CONFIDENCE, Double.class, null);
	}


	/**
	 * Write the confidence for a Segment.
	 *
	 * @param value
	 *            a Double
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeConfidence(final Double value) {
		return this.applyOperator(Operator.WRITE, AxesAnnotator.PREFIX, AxesAnnotator.NS, AxesAnnotator.HAS_CONFIDENCE, Double.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Collection Id for a Resource
	 *
	 * @return a String
	 */
	public Value<String> readCollectionId() {
		return this.applyOperator(Operator.READ, null, AxesAnnotator.NS, AxesAnnotator.HAS_COLLECTION_ID, String.class, null);
	}


	/**
	 * Write the Frame Id for a Resource
	 *
	 * @param value
	 *            a String
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeFrameId(final String value) {
		return this.applyOperator(Operator.WRITE, AxesAnnotator.PREFIX, AxesAnnotator.NS, AxesAnnotator.HAS_FRAME_ID, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Frame Id for a Resource
	 *
	 * @return a String
	 */
	public Value<String> readFrameId() {
		return this.applyOperator(Operator.READ, null, AxesAnnotator.NS, AxesAnnotator.HAS_FRAME_ID, String.class, null);
	}


	/**
	 * Write the Collection Id for a Resource
	 *
	 * @param value
	 *            a String
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeCollectionId(final String value) {
		return this.applyOperator(Operator.WRITE, AxesAnnotator.PREFIX, AxesAnnotator.NS, AxesAnnotator.HAS_COLLECTION_ID, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Shot Id for a Resource
	 *
	 * @return a String
	 */
	public Value<String> readShotId() {
		return this.applyOperator(Operator.READ, null, AxesAnnotator.NS, AxesAnnotator.HAS_SHOT_ID, String.class, null);
	}


	/**
	 * Write the Shot Id for a Resource
	 *
	 * @param value
	 *            a String
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeShotId(final String value) {
		return this.applyOperator(Operator.WRITE, AxesAnnotator.PREFIX, AxesAnnotator.NS, AxesAnnotator.HAS_SHOT_ID, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Speech Id for a Resource
	 *
	 * @return a String
	 */
	public Value<String> readSpeechId() {
		return this.applyOperator(Operator.READ, null, AxesAnnotator.NS, AxesAnnotator.HAS_SPEECH_ID, String.class, null);
	}


	/**
	 * Write the Speech Id for a Resource
	 *
	 * @param value
	 *            a String
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeSpeechId(final String value) {
		return this.applyOperator(Operator.WRITE, AxesAnnotator.PREFIX, AxesAnnotator.NS, AxesAnnotator.HAS_SPEECH_ID, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Video Id for a Resource
	 *
	 * @return a String
	 */
	public Value<String> readVideoId() {
		return this.applyOperator(Operator.READ, null, AxesAnnotator.NS, AxesAnnotator.HAS_VIDEO_ID, String.class, null);
	}


	/**
	 * Write the Video Id for a Resource
	 *
	 * @param value
	 *            a String
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeVideoId(final String value) {
		return this.applyOperator(Operator.WRITE, AxesAnnotator.PREFIX, AxesAnnotator.NS, AxesAnnotator.HAS_VIDEO_ID, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the Limas Id for a Resource (WebLab Resource, Segment, Entity...).
	 *
	 * @return a String
	 */
	public Value<String> readLimasId() {
		return this.applyOperator(Operator.READ, null, AxesAnnotator.NS, AxesAnnotator.HAS_LIMAS_ID, String.class, null);
	}


	/**
	 * Write the Limas for a Resource (WebLab Resource, Segment, Entity...).
	 *
	 * @param value
	 *            a String
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public PieceOfKnowledge writeLimasId(final String value) {
		return this.applyOperator(Operator.WRITE, AxesAnnotator.PREFIX, AxesAnnotator.NS, AxesAnnotator.HAS_LIMAS_ID, String.class, value).getIsAnnotatedOn();
	}


	/**
	 * Read the folder listener number of docs in total
	 *
	 * @return an integer
	 */
	public final Value<Integer> readNbDocsTotal() {
		return this.applyOperator(Operator.READ, null, AxesAnnotator.NS, AxesAnnotator.NB_OF_DOCS_TOTAL, Integer.class, null);
	}


	/**
	 * Write the folder listener number of docs in total
	 *
	 * @param value
	 *            an integer
	 * @return The {@link PieceOfKnowledge} in which the statement has been written.
	 */
	public final PieceOfKnowledge writeNbDocsTotal(final Integer value) {
		return this.applyOperator(Operator.WRITE, AxesAnnotator.PREFIX, AxesAnnotator.NS, AxesAnnotator.NB_OF_DOCS_TOTAL, Integer.class, value).getIsAnnotatedOn();
	}


}
