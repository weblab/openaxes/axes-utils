package eu.axes.utils;

import java.net.URI;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.Video;
import org.ow2.weblab.rdf.Value;



/**
 * Simple methods to ease handling naming scheme within WebLab documents.
 *
 * @author ymombrun
 * @date 2012-05-15
 */
public final class NamingSchemeUtils {


	private static final char CNT_SEP = ':';


	private static final Log LOG = LogFactory.getLog(NamingSchemeUtils.class);


	private static final char SEP = '/';


	/**
	 * @param annotator
	 *            The annotator to be read
	 * @param resource
	 *            The resource used to get the URI and type
	 * @return The image, text or video id, depending on the type of resource
	 * @throws WebLabCheckedException
	 *             If resource type is not handled
	 */
	public static String getAxesId(final AxesAnnotator annotator, final Resource resource) throws WebLabCheckedException {
		if (resource instanceof Image) {
			return NamingSchemeUtils.getAxesImageId(annotator, resource);
		} else if (resource instanceof Text) {
			return NamingSchemeUtils.getAxesTextId(annotator, resource);
		} else if ((resource instanceof Video) || (resource instanceof Document)) {
			return NamingSchemeUtils.getAxesVideoId(annotator, resource);
		}
		throw new WebLabCheckedException("The resource " + resource.getUri() + " is of type (" + resource.getClass() + ") which is not handled for AXES ids.");
	}


	/**
	 * @param annotator
	 *            The annotator to be read
	 * @param img
	 *            The image resource
	 * @return The id of the image (/collectionId/videoId/shotId/frameId)
	 * @throws WebLabCheckedException
	 */
	public static String getAxesImageId(final AxesAnnotator annotator, final Resource img) throws WebLabCheckedException {
		return NamingSchemeUtils.SEP + NamingSchemeUtils.getCollectionId(annotator, img) + NamingSchemeUtils.SEP + NamingSchemeUtils.getVideoId(annotator, img) + NamingSchemeUtils.SEP
				+ NamingSchemeUtils.getShotId(annotator, img) + NamingSchemeUtils.SEP + NamingSchemeUtils.getFrameId(annotator, img);
	}


	/**
	 * @param annotator
	 *            The annotator to be read
	 * @param txt
	 *            The text resource
	 * @return The id of the text (/collectionId/videoId/speechId)
	 * @throws WebLabCheckedException
	 */
	public static String getAxesTextId(final AxesAnnotator annotator, final Resource txt) throws WebLabCheckedException {
		return NamingSchemeUtils.SEP + NamingSchemeUtils.getCollectionId(annotator, txt) + NamingSchemeUtils.SEP + NamingSchemeUtils.getVideoId(annotator, txt) + NamingSchemeUtils.SEP
				+ NamingSchemeUtils.getSpeechId(annotator, txt);
	}


	/**
	 * @param annotator
	 *            The annotator to be read
	 * @param res
	 *            The document or video resource
	 * @return The id of the video (/collectionId/videoId)
	 * @throws WebLabCheckedException
	 */
	public static String getAxesVideoId(final AxesAnnotator annotator, final Resource res) throws WebLabCheckedException {
		return NamingSchemeUtils.SEP + NamingSchemeUtils.getCollectionId(annotator, res) + NamingSchemeUtils.SEP + NamingSchemeUtils.getVideoId(annotator, res);
	}


	/**
	 * @param annotator
	 *            The annotator to be read
	 * @param res
	 *            The resource
	 * @return The id of the collection only (collectionId)
	 * @throws WebLabCheckedException
	 */
	public static String getCollectionId(final AxesAnnotator annotator, final Resource res) throws WebLabCheckedException {
		final Value<String> collectionId = annotator.readCollectionId();
		return NamingSchemeUtils.getFirstValue(collectionId, "axes:hasCollectionId", res.getUri());
	}


	/**
	 * @param annotator
	 *            The annotator to be read
	 * @param res
	 *            The image resource
	 * @return The id of the frame only (frameId)
	 * @throws WebLabCheckedException
	 */
	public static String getFrameId(final AxesAnnotator annotator, final Resource res) throws WebLabCheckedException {
		final Value<String> frameId = annotator.readFrameId();
		return NamingSchemeUtils.getFirstValue(frameId, "axes:hasFrameId", res.getUri());
	}


	/**
	 * @param collectionId
	 *            The id of the collection
	 * @param videoId
	 *            The id of the video
	 * @param ext
	 *            The extension of the requested image file
	 * @param shotId
	 *            The id of the shot
	 * @param frameId
	 *            The id of the frame
	 * @return The normalised content uri
	 */
	public static URI getImageResourceURI(final String collectionId, final String videoId, final String ext, final String shotId, final String frameId) {
		return URI.create("urn:axes:content:" + collectionId + CNT_SEP + videoId + CNT_SEP + shotId + CNT_SEP + frameId + "." + ext);
	}


	/**
	 * @param annotator
	 *            The annotator to be read
	 * @param res
	 *            The video resource
	 * @return The id of the shot only (shotId)
	 * @throws WebLabCheckedException
	 */
	public static String getShotId(final AxesAnnotator annotator, final Resource res) throws WebLabCheckedException {
		final Value<String> shotId = annotator.readShotId();
		return NamingSchemeUtils.getFirstValue(shotId, "axes:hasShotId", res.getUri());
	}


	/**
	 * @param annotator
	 *            The annotator to be read
	 * @param res
	 *            The text resource
	 * @return The id of the speech only (speechId)
	 * @throws WebLabCheckedException
	 */
	public static String getSpeechId(final AxesAnnotator annotator, final Resource res) throws WebLabCheckedException {
		final Value<String> speechId = annotator.readSpeechId();
		return NamingSchemeUtils.getFirstValue(speechId, "axes:hasSpeechId", res.getUri());
	}


	/**
	 * @param annotator
	 *            The annotator to be read
	 * @param res
	 *            The resource
	 * @return The id of the video only (videoId)
	 * @throws WebLabCheckedException
	 */
	public static String getVideoId(final AxesAnnotator annotator, final Resource res) throws WebLabCheckedException {
		final Value<String> videoId = annotator.readVideoId();
		return NamingSchemeUtils.getFirstValue(videoId, "axes:hasVideoId", res.getUri());
	}


	/**
	 * @param collectionId
	 *            The id of the collection
	 * @param videoId
	 *            The id of the video
	 * @param ext
	 *            The extension of the requested image file
	 * @return The normalised content uri
	 */
	public static URI getVideoResourceURI(final String collectionId, final String videoId, final String ext) {
		return URI.create("urn:axes:content:" + collectionId + CNT_SEP + videoId + "." + ext);
	}


	protected static String getFirstValue(final Value<String> value, final String property, final String resUri) throws WebLabCheckedException {
		if (!value.hasValue()) {
			throw new WebLabCheckedException("The " + property + " is not annotated on " + resUri + ".");
		}
		if (value.size() > 1) {
			NamingSchemeUtils.LOG.warn("More than one " + property + " annotation found on " + resUri + ". Using first one");
		}
		return value.firstTypedValue();
	}


}
