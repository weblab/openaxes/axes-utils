package ch.ebu.www.ebucore;

import java.io.File;
import java.net.URI;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.rdf.Value;

/**
 * @author ymombrun
 */
public class AnnotatorTest {


	@Test
	public void testAnnotatorWrite() throws Exception {
		final Document docInit = WebLabResourceFactory.createResource("test", "ebucore", Document.class);

		final EBUCoreAnnotator ebucoreWriter = new EBUCoreAnnotator(docInit);

		final String mainTitle = "maintitle";
		ebucoreWriter.writeMainTitle(mainTitle);

		final URI publiEvent = new URI("pubevent");
		ebucoreWriter.writePubliEvent(publiEvent);

		final URI colourSpace = new URI("colourspace");
		ebucoreWriter.writeColourSpace(colourSpace);

		final URI keyword = new URI("keyword");
		ebucoreWriter.writeKeyword(keyword);

		final URI subject = new URI("subject");
		ebucoreWriter.writeSubject(subject);

		final URI genre = new URI("genre");
		ebucoreWriter.writeGenre(genre);

		final URI contributor = new URI("contributor");
		ebucoreWriter.writeContributor(contributor);

		final URI isCoveredBy = new URI("iscoveredby");
		ebucoreWriter.writeIsCoveredBy(isCoveredBy);

		final String summary = "summary";
		ebucoreWriter.writeSummary(summary);

		final Date published = Calendar.getInstance().getTime();
		ebucoreWriter.writePublished(published);

		final URI publiChannel = new URI("publichannel");
		ebucoreWriter.writePubliChannel(publiChannel);

		final String keywordName = "keywordname";
		ebucoreWriter.writeKeywordName(keywordName);

		final String genreName = "genrename";
		ebucoreWriter.writeGenreName(genreName);

		final String name = "name";
		ebucoreWriter.writeName(name);

		final URI role = new URI("role");
		ebucoreWriter.writeRole(role);

		final URI rightsHolder = new URI("rightsholder");
		ebucoreWriter.writeRightsHolder(rightsHolder);

		final File file = new File("target/test2.xml");
		new WebLabMarshaller(true).marshalResource(docInit, file);

		final Document doc = new WebLabMarshaller(true).unmarshal(file, Document.class);

		final EBUCoreAnnotator ebucoreReader = new EBUCoreAnnotator(doc);

		final Value<String> readMainTitleValue = ebucoreReader.readMainTitle();
		Assert.assertNotNull(readMainTitleValue);
		Assert.assertEquals(1, readMainTitleValue.size());
		Assert.assertEquals(mainTitle, readMainTitleValue.firstTypedValue());

		final Value<String> readSummaryValue = ebucoreReader.readSummary();
		Assert.assertNotNull(readSummaryValue);
		Assert.assertEquals(1, readSummaryValue.size());
		Assert.assertEquals(summary, readSummaryValue.firstTypedValue());

		final Value<String> readKeywordNameValue = ebucoreReader.readKeywordName();
		Assert.assertNotNull(readKeywordNameValue);
		Assert.assertEquals(keywordName, readKeywordNameValue.firstTypedValue());

		final Value<String> readGenreNameValue = ebucoreReader.readGenreName();
		Assert.assertNotNull(readGenreNameValue);
		Assert.assertEquals(genreName, readGenreNameValue.firstTypedValue());

		final Value<String> readNameValue = ebucoreReader.readName();
		Assert.assertNotNull(readNameValue);
		Assert.assertEquals(name, readNameValue.firstTypedValue());

		final Value<URI> readPubliEventValue = ebucoreReader.readPubliEvent();
		Assert.assertNotNull(readPubliEventValue);
		Assert.assertEquals(publiEvent, readPubliEventValue.firstTypedValue());

		final Value<URI> readColourSpaceValue = ebucoreReader.readColourSpace();
		Assert.assertNotNull(readColourSpaceValue);
		Assert.assertEquals(colourSpace, readColourSpaceValue.firstTypedValue());

		final Value<URI> readKeywordValue = ebucoreReader.readKeyword();
		Assert.assertNotNull(readKeywordValue);
		Assert.assertEquals(keyword, readKeywordValue.firstTypedValue());

		final Value<URI> readSubjectValue = ebucoreReader.readSubject();
		Assert.assertNotNull(readSubjectValue);
		Assert.assertEquals(subject, readSubjectValue.firstTypedValue());

		final Value<URI> readGenreValue = ebucoreReader.readGenre();
		Assert.assertNotNull(readGenreValue);
		Assert.assertEquals(genre, readGenreValue.firstTypedValue());

		final Value<URI> readContributorValue = ebucoreReader.readContributor();
		Assert.assertNotNull(readContributorValue);
		Assert.assertEquals(contributor, readContributorValue.firstTypedValue());

		final Value<URI> readIsCoveredByValue = ebucoreReader.readIsCoveredBy();
		Assert.assertNotNull(readIsCoveredByValue);
		Assert.assertEquals(isCoveredBy, readIsCoveredByValue.firstTypedValue());

		final Value<Date> readPublishedValue = ebucoreReader.readPublished();
		Assert.assertNotNull(readPublishedValue);
		Assert.assertEquals(published.toString(), readPublishedValue.firstTypedValue().toString());

		final Value<URI> readPubliChannelValue = ebucoreReader.readPubliChannel();
		Assert.assertNotNull(readPubliChannelValue);
		Assert.assertEquals(publiChannel, readPubliChannelValue.firstTypedValue());

		final Value<URI> readRoleValue = ebucoreReader.readRole();
		Assert.assertNotNull(readRoleValue);
		Assert.assertEquals(role, readRoleValue.firstTypedValue());

		final Value<URI> readRightsHolderValue = ebucoreReader.readRightsHolder();
		Assert.assertNotNull(readRightsHolderValue);
		Assert.assertEquals(rightsHolder, readRightsHolderValue.firstTypedValue());
	}


	@Test
	public void testAnnotatorRead() throws Exception {

		final File file = new File("src/test/resources/classicTest.xml");
		final Document doc = new WebLabMarshaller(true).unmarshal(file, Document.class);

		final EBUCoreAnnotator ebucoreReader = new EBUCoreAnnotator(doc);

		final Set<String> readMainTitleValue = ebucoreReader.getMainTitles();
		Assert.assertNotNull(readMainTitleValue);
		Assert.assertEquals(1, readMainTitleValue.size());
		System.out.println("Main titles: " + readMainTitleValue);

		final Set<String> readSummaryValue = ebucoreReader.getSummaries();
		Assert.assertNotNull(readSummaryValue);
		Assert.assertEquals(1, readSummaryValue.size());
		System.out.println("Summaries: " + readSummaryValue);

		final Set<String> readSubjectValue = ebucoreReader.getSubjects();
		Assert.assertNotNull(readSubjectValue);
		Assert.assertEquals(1, readSubjectValue.size());
		System.out.println("Subjects: " + readSubjectValue);

		final Set<String> readKeywordValue = ebucoreReader.getKeywords();
		Assert.assertNotNull(readKeywordValue);
		Assert.assertEquals(1, readKeywordValue.size());
		System.out.println("Keywords: " + readKeywordValue);

		final Set<String> readGenreValue = ebucoreReader.getGenres();
		Assert.assertNotNull(readGenreValue);
		Assert.assertEquals(1, readGenreValue.size());
		System.out.println("Genres: " + readGenreValue);

		final Set<String> readColourSpaceValue = ebucoreReader.getColourSpaces();
		Assert.assertNotNull(readColourSpaceValue);
		Assert.assertEquals(1, readColourSpaceValue.size());
		System.out.println("Colour spaces: " + readColourSpaceValue);

		final Map<Date, Set<String>> readPubliEventValue = ebucoreReader.getPubliEvents();
		Assert.assertNotNull(readPubliEventValue);
		Assert.assertEquals(1, readPubliEventValue.size());
		System.out.println("Publication events: " + readPubliEventValue);

		final Map<String, Set<String>> readContributorValue = ebucoreReader.getContributors();
		Assert.assertNotNull(readContributorValue);
		Assert.assertEquals(1, readContributorValue.size());
		System.out.println("Contributors: " + readContributorValue);

		final Map<String, Set<String>> readRightsValue = ebucoreReader.getRights();
		Assert.assertNotNull(readRightsValue);
		Assert.assertEquals(1, readRightsValue.size());
		System.out.println("Rights: " + readRightsValue);
	}

}
